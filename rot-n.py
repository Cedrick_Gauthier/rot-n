# ============================= rot-n.py ========================================
# Redige par Cédrick Gauthier, 2018-01-02
#================================================================================
# Description: 
#     Sole rotate "encryptions"
#================================================================================

import sys

def rotChar(inChar, rotNumber):
    inCode = ord(inChar)
    outCode = 0

    #lowercase letters___________________________________________________________
    if inCode >= 97 and inCode <= 122:
        outCode = inCode - rotNumber
        if outCode < 97:
            outCode = 122 - abs(97 - outCode)

    #uppercase letters___________________________________________________________
    elif inCode >= 65 and inCode <= 90:
        outCode = inCode - rotNumber
        if outCode < 65:
            outCode = 90 - abs(65 - outCode)

    #none letters charracters____________________________________________________
    else:
        outCode = inCode

    return chr(outCode);

def rotText(inText, rotNumber):
    outStr = ""
    for i in range(0, len(inText)):
        outStr = outStr + rotChar(inText[i], rotNumber)

    return outStr;

inFile = open(sys.argv[1], "r")
inText = list(inFile.read())
inFile.close()

if len(sys.argv) == 2:
    for rotNumber in range(1, 25):
        print("rot " + str(rotNumber) + ": \n")
        print(rotText(inText, rotNumber))

elif len(sys.argv) == 3:
    print(rotText(inText, int(sys.argv[2])))

else:
    print("syntax: python3.5 rot-n.py [path] [*rotNumber]")
